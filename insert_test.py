from selenium import webdriver 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC 
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.firefox.options import Options
from base64 import decodestring
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
binary = FirefoxBinary('/usr/bin/firefox/firefox')
from pprint import pprint


import base64
import argparse
import time 
import os
import datetime
import psutil
import gc
import sys
import re
import requests
import urllib
import socket
proxies = {'http': '104.223.115.48:3128', 'https': '104.223.115.48:3128'}
url_api = "http://localhost/bakeca-backend/index.php/anuncio/"

env = socket.getfqdn()


firefox_profile = webdriver.FirefoxProfile()
firefox_options = Options()
firefox_options.headless = False
firefox_capabilities = webdriver.DesiredCapabilities.FIREFOX
firefox_capabilities['marionette'] = True
proxy = "104.223.115.48:3128"

if(env=='dolci5-ny'):
	firefox_profile.set_preference("network.proxy.type", 1)
	firefox_profile.set_preference("network.proxy.http", "104.223.115.48")
	firefox_profile.set_preference("network.proxy.http_port", 3128)
	firefox_profile.update_preferences() 

	firefox_capabilities['proxy'] = {
		"proxyType": "MANUAL",
		"httpProxy": proxy,
		"ftpProxy": proxy,
		"sslProxy": proxy
	}

	
	
driver = webdriver.Firefox(firefox_binary=binary, executable_path="/var/tmp/geckodriver", firefox_profile=firefox_profile, options=firefox_options,capabilities=firefox_capabilities)

#CHROME
'''
os.environ["PATH"] += os.pathsep + r"C:\\chromedriver"
chrome_profile = webdriver.Chrome()
chrome_options = Options()
chrome_options.headless = True

driver = webdriver.Chrome()  # Optional argument, if not specified will search path.

'''
'''
options = Options() 
options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
options.add_argument("disable-infobars")
options.add_argument("log-level=3")
options.add_argument("disable-popup-blocking")
options.add_argument('disable-web-security')
options.add_argument('--disable-application-cache')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome(options=options, executable_path=os.getcwd()+"\\drivers\\chromedriver.exe")
'''

def waitForElement(selector, time):
	WebDriverWait(driver, time).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

def elementIsClicable(selector, time):
	WebDriverWait(driver, time).until(EC.element_to_be_clickable((By.CSS_SELECTOR,selector)))

def log(acao, tipo = 'LOG', result= 'SUCCESS'):
	try:
		now = datetime.datetime.now()
		now = now.strftime("%d/%m/%Y %H:%M:%S")
		now = str(now)
		gc.collect()
		process = psutil.Process(os.getpid())
		process = process.memory_info().rss/1024
		print(tipo+'['+now+']'+ '-> '+ acao + ' ['+result+']')


	except Exception as ex:
		print(str(ex))
		# raise

def save_log(id_queue="", id_pedido="", log=""):
	url_api = "http://68.183.110.3/bakeca-backend/index.php/anuncio/"
	try:
		requests.post(url_api + '/log', data={'id_queue':id_queue, 'id_pedido':id_pedido, 'log':log},proxies=proxies)
	except Exception as ex:
		print('ERROR -> ' + ex)

def send_notification(id_agente, msg):
	try:
		requests.post('http://68.183.110.3/bakeca-backend/index.php/anuncio/send_notification/',  data={'id_agente':id_agente,'msg':msg},proxies=proxies)
	except Exception as ex:
		save_log('ERROR -> ' + ex)
		print('ERROR -> ' + ex)


def waitElementor(xpath,time):
    element_present = EC.presence_of_element_located((By.CSS_SELECTOR, xpath))
    WebDriverWait(driver, time).until(element_present)


def init(phpsessid = "", citta = "", categoria = "", indrizzo = "", cap = "", zona = "", titolo = "", testo = "", image_1 = "", image_2 = "", image_3 = "", image_4 = "", image_5 = "", image_6 = "", whatsapp=0, eta="", tel="", id_fila="", id_annuncio="", crop_position = "", total_img = "", crop_axys = "", id_pedido="", id_agente="", tipo_ctt="", giorni_select="", fascia_selected=""):
	url = 'https://aosta.bakecaincontrii.com/fe/main.php?page=post_insert'
	url_changed = url + '&adding=1'
	images = []
	url_api = "http://68.183.110.3/bakeca-backend/index.php/anuncio/"

	log('IDP -> ' + str(phpsessid))
	try:
		log(id_fila, id_pedido, 'INSERINDO COOKIE')
		driver.get('https://www.bakecaincontrii.com/null/')
		driver.add_cookie({'name' : 'adult_cookie', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
		driver.add_cookie({'name' : 'privacy_cookie', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
		driver.add_cookie({'name' : 'no_more_promo_vc', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
		driver.add_cookie({'name' : 'no_more_push_premium', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
		driver.add_cookie({'name' : 'PHPSESSID', 'value' : phpsessid, 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
		log(id_fila, id_pedido, 'COOKIE INSERIDO')
		driver.get(url)
		save_log(id_fila, id_pedido, 'PAGINA DO ANUNCIO CARREGADA')
		try: 
			driver.find_element_by_css_selector('#promo-premium-actions a').click()
		except:
			pass
		try:
			waitElementor('#accetto', 20)
			log('PAGINA CARREGADA COM SUCESSO')

			try:
				#driver.find_element_by_xpath('//*[@id="accetto"]').click()

				try:
					waitElementor('.ins-box-titolo', 5)

					# Declarando Campos
					log('COLETANDO CAMPOS DO FORM')
					_citta = driver.find_element_by_css_selector('#citta-ins')
					_indrizzo = driver.find_element_by_css_selector('#indirizzo-ins')
					_cap = driver.find_element_by_css_selector('#cap-ins')
					_zona = driver.find_element_by_css_selector('#zona-ins')
					_titolo = driver.find_element_by_css_selector('#titolo-ins')
					_testo = driver.find_element_by_css_selector('#testo-ins')
					_eta = driver.find_element_by_css_selector('#eta-ins')
					_tel = driver.find_element_by_css_selector('#tel-ins')


					# Limpando todos os Campos
					log('LIMPANDO CAMPOS DO FORM')
					if categoria:
						select = Select(driver.find_element_by_xpath("""//select[@id='categoria-ins']"""))
						select.select_by_value(categoria)

					if citta:
						select = Select(driver.find_element_by_xpath("""//select[@id='citta-ins']"""))
						select.select_by_value(citta)

					if indrizzo:
						_indrizzo.send_keys(indrizzo)

					if cap:
						_cap.send_keys(cap)

					if zona:
						_zona.send_keys(zona)

					if titolo:
						log("TITOLO MOD")
						regex = r"<br\s*[\/]?>"
						titolo = re.sub(regex, '\n', titolo)
						titolo = titolo.replace("&#039;", "'")
						titolo = titolo.replace('"', "'")
						#driver.execute_script("document.getElementsByName('titolo')[0].value='"+titolo+"'")
						driver.find_element_by_name("titolo").send_keys(titolo)

					if testo:
						log("TESTO MOD")
						regex = r"<br\s*[\/]?>"
						testo = re.sub(regex, "\n", testo)
						testo = testo.replace("&#039;", "'")
						testo = testo.replace('"', "'")
						driver.find_element_by_name("testo").send_keys(testo)

					if eta:
						log("IDADE MOD")
						_eta.send_keys(eta)

					if tel:
						_tel.send_keys(tel)
					
					driver.find_element_by_css_selector('#privacy-ins').click()

					# Preenchendo Campos
					log('PREENCHENDO FORM')

					if whatsapp == '1':
						log('COLOCANDO WHATSAPP')
						try:
							chekbox = driver.find_element_by_xpath('//*[@id="whatsapp"]')

							if chekbox.is_selected():
								log('WHATSAPP JA COLOCADO')
								pass

							else:
								log('WHATSAPP COLOCADO AGORA')
								chekbox.click()

						except Exception as ex:
							pass

					if id_pedido:
						log('NOVA IMAGEM')	
						imgs = []					
						if image_1 != "":
							imgs.append(image_1)
						if image_2 != "":
							imgs.append(image_2)
						if image_3 != "":
							imgs.append(image_3)
						if image_4 != "":
							imgs.append(image_4)
						if image_5 != "":
							imgs.append(image_5)
						if image_6 != "":
							imgs.append(image_6)
							
						path =  os.getcwd()
						for img in imgs:
							if str(img) != "None":
								tmp_name = path+'/'+str(time.time())+'.jpg'
								urllib.urlretrieve('https://res.cloudinary.com/do2gudpz7/image/upload/'+str(img), tmp_name)
								images.append(str(tmp_name))

						time.sleep(2)
						pprint(imgs)	
						pprint(images)

						try:
							log('ENVIANDO IMAGENS')
							cnt = 0
							#log("teste:"+str(cnt)+" "+str(crop_position))
							#log("RANGE: "+str(len(images)))
							for i in range(len(images)):	
								
								if cnt != int(crop_position):
									#log('IMAGE -> '+str(images[i]))
									try:
										
										if str(images[i]) != "None":
											driver.find_element_by_id("upfile").send_keys(images[i])
											log("ENVIOU "+images[i])
										try:
											WebDriverWait(driver, 10).until(EC.url_changes(url_changed))
											time.sleep(1)
											os.remove(images[i])
											#log("REMOVE "+images[i])
											try:
												waitForElement('html body.ins div.lb-contenitore div.lb-finestra div.row.bg-bianco div.lb-contenuto.form-default div.row div.col.unico.center-align p',1)
												# Aqui tratado o erro de Badposition
												text_error = driver.find_element_by_css_selector('html body.ins div.lb-contenitore div.lb-finestra div.row.bg-bianco div.lb-contenuto.form-default div.row div.col.unico.center-align p').text
												log('ERRO  -> '+str(text_error))
												save_log(id_fila, id_pedido, 'ERRO 1 -> '+str(ex))
												requests.get(url_api + '/confirmar/'+str(id_fila)+'/3',proxies=proxies)
												requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
												sys.exit(1)
											except Exception as ex:
												pass
										except Exception as ex:
											log(str(ex), 'ERRO', 'ERROR')
											save_log(id_fila, id_pedido, 'ERRO 2 -> '+str(ex))
											requests.get(url_api + '/confirmar/'+str(id_fila)+'/4',proxies=proxies)			
											requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
											sys.exit(1)
											raise

									except Exception as ex:
										log(str(ex), 'ERRO', 'ERROR')
										save_log(id_fila, id_pedido, 'ERRO 3 -> '+str(ex))
										requests.get(url_api + '/confirmar/'+str(id_fila)+'/5',proxies=proxies)
										requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
										sys.exit(1)		
										raise
								else:
									log("Imagem "+str(cnt)+" pulada")
								
								cnt = cnt+1
						except Exception as ex:
							log('ERRO 4  ->' + str(ex))
							driver.quit()
							sys.exit(1)	
					#time.sleep(2)
					#os.remove(path)
					driver.find_element_by_css_selector('#submit-ins').click()		
					log('MODIFICANDO ANUNCIO...')
					waitForElement('#promuovi',10)
					
			
					if tipo_ctt:
						try:
							#driver.execute_script("$('#giorni-select').prop('disabled', false).trigger('change');")
							select = Select(driver.find_element_by_css_selector('#tipo_ctt'))
							select.select_by_value(tipo_ctt)
							driver.execute_script("$('#tipo_ctt').trigger('change');")
							elementIsClicable('#giorni-select option[value="'+giorni_select+'"]', 10)
						except Exception as ex:
							log('ERRO tipo: ' + str(ex))
							pass

						

					if giorni_select:
						try:
							driver.execute_script("$('#fascia-select').prop('disabled', false).trigger('change');")
							select = Select(driver.find_element_by_css_selector('#giorni-select'))
							select.select_by_value(giorni_select)
							elementIsClicable('#fascia-select option[value="'+fascia_selected+'"]', 10)
						except Exception as ex:
							log('ERRO fascia: ' + str(ex))
							pass

					if fascia_selected:
						select = Select(driver.find_element_by_css_selector('#fascia-select'))
						select.select_by_value(fascia_selected)
					try:
						time.sleep(3)
						crop_img = int(crop_position)
						log("THUMB SELECTED "+str(crop_img)+" - "+str(images[crop_img]))
						for image in images:
								log('IMAGE '+str(crop_img)+'-> '+str(image))
						driver.find_element_by_id("img_crop_upload").send_keys(images[crop_img])
						WebDriverWait(driver, 10).until(EC.url_changes(url_changed))
						log('IMAGEM DE CAPA ENVIADA')
						time.sleep(3)
					except Exception as ex:
							log('ERRO thumb: ' + str(ex))
							pass

					if crop_position:
						log('CROPANDO IMAGE')
						try:
							crop_axys = crop_axys.split(",")

							driver.execute_script('document.querySelector("input[name=preview_x]").value='+str(crop_axys[0]))
							log('POSITION x_src ->' + str(crop_axys[0]))

							driver.execute_script('document.querySelector("input[name=preview_y]").value='+str(crop_axys[1]))
							log('POSITION y_src ->' + str(crop_axys[1]))

							driver.execute_script('document.querySelector("input[name=preview_width]").value='+str(crop_axys[2]))
							log('POSITION w_src ->' + str(crop_axys[2]))

							driver.execute_script('document.querySelector("input[name=preview_height]").value='+str(crop_axys[3]))
							log('POSITION h_src ->' + str(crop_axys[3]))

							driver.execute_script('document.querySelector("input[name=to_recut]").value=1')
							driver.execute_script('document.querySelector("input[name=preview]").value=1')
							driver.execute_script('document.querySelector("input[name=image_replaced]").value=1')

							save_log(id_fila, id_pedido, 'CROP REALIZADO COM EXITO')


						except Exception as ex:
							log('ERRO NO CROP' + str(ex))
							save_log(id_fila, id_pedido, 'ERRO NO CROP -> '+str(ex))
							#requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
							#driver.quit()
							#sys.exit(1)
							raise	

					try:
						driver.find_element_by_css_selector('#promuovi').click()
						log('clique promove')

						try:
							waitForElement('#acquista', 10)
							idp = driver.find_element_by_css_selector('.ins-messaggio a').get_attribute("href").split('idp=')[1]
							manage_url = driver.find_element_by_css_selector('.ins-messaggio a').get_attribute("href").replace('ctt_choice', 'post_manage').replace('ctt_pay','ctt_choice')
							log(manage_url)
							driver.get(manage_url)
							save_log(id_fila, id_pedido, 'ANUNCIO CRIADO COM SUCESSO: '+manage_url)
							#sys.exit(1)
							try:
								waitForElement('#immagine-anteprima-ctt > img', 10)
								log('GET THUMBNAIL')
								image_thumb = driver.find_element_by_css_selector('#immagine-anteprima-ctt > img').get_attribute('src')

								b64_thumb = base64.b64encode(requests.get(image_thumb,proxies=proxies).content)
								log('THUMB -> BASE64')
								
								log('ANUNCIO CRIADO COM SUCESSO')
								try:
									requests.post(url_api + '/atualizar_link/' + id_annuncio,proxies=proxies)
									requests.post(url_api + '/atualizar_thumb/' + str(idp),proxies=proxies, data={'img':b64_thumb})
									requests.post(url_api + '/update_anuncio/' + id_annuncio + '/' + str(idp) + '/0',proxies=proxies)
									requests.get(url_api + '/confirmar/'+str(id_fila)+'/1',proxies=proxies)
									msg = "ANUNCIO CRIADO COM SUCESSO " + id_fila + "," + id_pedido +" Link: "+manage_url
									#send_notification(id_agente, msg)
								except Exception as ex:
									log(str(ex), 'ERRO', 'ERROR')
									save_log(id_fila, id_pedido, 'ERRO FINALIZACAO  -> '+str(ex))
									driver.get('https://varese.bakecaincontrii.com/fe/main.php?page=post_delete_ajax&idp='+ str(idp))
									#requests.get(url_api + '/confirmar/'+str(id_fila)+'/0',proxies=proxies)
									requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
									msg = "ERRO NA REQUEST " + id_fila + "," + id_pedido +" Link: "+manage_url
									send_notification(id_agente, msg)
									driver.quit()
									sys.exit(1)
									#pass

							except Exception as ex:
								log(str(ex), 'ERRO', 'ERROR')
								save_log(id_fila, id_pedido, 'ERRO REQUISICAO MINIATURA  -> '+str(ex))
								requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
								driver.get('https://varese.bakecaincontrii.com/fe/main.php?page=post_delete_ajax&idp='+ str(idp))
								driver.quit()
								#sys.exit(1)

						except Exception as ex:
							log('PROBLEMA AO CRIAR O ANUNCIO ->' + str(ex))
							save_log(id_fila, id_pedido, 'PROBLEMA AO CRIAR O ANUNCIO -> '+str(ex))
							#requests.get(url_api + '/confirmar/'+str(id_fila)+'/2',proxies=proxies)
							requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
							msg = "FALHA AO CADASTRAR ANUNCIO: Texto, titulo ou imagens. Fila: "+str(id_fila)+" Pedido:"+id_pedido
							send_notification(id_agente, msg)
							driver.quit()
							sys.exit(1)

					except Exception as ex:
						log('ERRO BOTAO PROMOVE  ->' + str(ex))
						save_log(id_fila, id_pedido, 'ERRO BOTAO PROMOVE -> '+str(ex))
						pass
						
				except Exception as ex:
					msg = "FALHA AO CADASTRAR ANUNCIO: Texto, titulo ou imagens. \n Fila: "+str(id_fila)+" Pedido:"+id_pedido+" \n Motivo: "
					badwords = driver.find_element_by_css_selector('span.text-error')
					if badwords:
						error_detail = 'ERRORE: Controla il titolo/testo (Parole Proibite e/o Numero di Telefono)'
						pass
					else:
						error_detail = 'ERRO 6 -> '+str(ex)
						pass
					save_log(id_fila, id_pedido, msg+error_detail)

					log(str(ex), 'ERRO', 'ERROR')
					requests.get(url_api + '/confirmar/'+str(id_fila)+'/0',proxies=proxies)
					requests.get(url_api + '/tentativa/'+str(id_fila))
					send_notification(id_agente, msg+error_detail)
					driver.quit()
					sys.exit(1)					

			except Exception as ex:
				pass
				log(str(ex), 'ERRO', 'ERROR')
				save_log(id_fila, id_pedido, 'ERRO 7 -> '+str(ex))
				#requests.get(url_api + '/tentativa/'+str(id_fila))
				driver.quit()
				#sys.exit(1)	

		except Exception as ex:
			pass
			log(str(ex), 'ERRO', 'ERROR')
			save_log(id_fila, id_pedido, 'ERRO 8 -> '+str(ex))
			requests.get(url_api + '/confirmar/'+str(id_fila)+'/0',proxies=proxies)
			requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
			msg = "PROBLEMA AO CRIAR O ANUNCIO 2 " + id_fila + "," + id_pedido
			send_notification(id_agente, msg)
			driver.quit()
			sys.exit(1)	

	except Exception as ex:
		log(str(ex), 'ERRO', 'FALHA AO CONECTAR')
		save_log(id_fila, id_pedido, 'ERRO CONEXAO -> '+str(ex))
		requests.get(url_api + '/confirmar/'+str(id_fila)+'/0',proxies=proxies)
		requests.get(url_api + '/tentativa/'+str(id_fila),proxies=proxies)
		driver.get('https://varese.bakecaincontrii.com/fe/main.php?page=post_delete_ajax&idp='+ str(idp))
		driver.quit()
		sys.exit(1)

log('INICIANDO')
parser = argparse.ArgumentParser()
parser.add_argument('--phpsessid', required=False)
parser.add_argument('--citta', required=False)
parser.add_argument('--categoria', required=False)
parser.add_argument('--indirizzo', required=False)
parser.add_argument('--cap', required=False)
parser.add_argument('--zona', required=False)
parser.add_argument('--titolo', required=False)
parser.add_argument('--testo', required=False)
parser.add_argument('--foto1', required=False)
parser.add_argument('--foto2', required=False)
parser.add_argument('--foto3', required=False)
parser.add_argument('--foto4', required=False)
parser.add_argument('--foto5', required=False)
parser.add_argument('--foto6', required=False)
parser.add_argument('--whatsapp', required=False)
parser.add_argument('--eta', required=False)
parser.add_argument('--tel', required=False)
parser.add_argument('--id_fila', required=True)
parser.add_argument('--id_annuncio', required=True)
parser.add_argument('--crop_img_pos', required=False)
parser.add_argument('--crop_img_axys', required=False)
parser.add_argument('--id_pedido', required=True)
parser.add_argument('--id_agente', required=True)
parser.add_argument('--tipo_ctt', required=True)
parser.add_argument('--giorni_select', required=True)
parser.add_argument('--fascia_selected', required=True)

args = parser.parse_args()

# ===========================================================================================
# COMO UTILIZAR
# utilizar atributos no shell como acima, ex: python run.py -idp=394023948203948 -indrizzo="EOFNPE WEOFNWPEOFN" ....

init(args.phpsessid, args.citta, args.categoria, args.indirizzo, args.cap, args.zona, args.titolo, args.testo, args.foto1, args.foto2, args.foto3, args.foto4, args.foto5, args.foto6, args.whatsapp, args.eta, args.tel, args.id_fila, args.id_annuncio, args.crop_img_pos, args.crop_img_axys, args.id_pedido, args.id_agente, args.tipo_ctt, args.giorni_select, args.fascia_selected)

driver.quit()