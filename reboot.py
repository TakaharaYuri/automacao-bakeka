import urllib
import subprocess
import sys
from base64 import decodestring
import base64
import time 
import requests
import os
import datetime
import gc
import psutil
import re
import glob


def save_log(id_queue="", id_pedido="", log=""):
	url_api = "http://68.183.110.3/bakeca-backend/index.php/anuncio/"
	try:
		requests.post(url_api + '/log', data={'id_queue':id_queue, 'id_pedido':id_pedido, 'log':log})
	except Exception as ex:
		print('ERROR -> ' + ex)

def str_clean(str):
	return re.sub('[^A-Za-z0-9]+', '', str)


def log(acao, tipo = 'LOG'):
	try:
		now = datetime.datetime.now()
		now = now.strftime("%d/%m/%Y %H:%M:%S")
		now = str(now)
		gc.collect()
		process = psutil.Process(os.getpid())
		process = process.memory_info().rss/1024

		print(tipo+'['+now+']'+ '-> '+ acao)
	except Exception as ex:
		log(str(ex))

def init():
	log('ENTRANDO NA API')
	path = "http://68.183.110.3/bakeca-backend/index.php/anuncio/get_pagar"
	try:
		retorno = requests.get(path).json()
		log('RETORNINHO -> ' + str(retorno['result']))
		 
		if (retorno['result'] ==  False):
			subprocess.Popen("pkill -9 firefox", shell=True)
			subprocess.Popen("pkill -9 geckodriver", shell=True)
			subprocess.Popen("pkill -9 python", shell=True)
			subprocess.Popen("python /var/www/automacao-bakeka/cron_pay.py", shell=True)
			requests.get('http://68.183.110.3/bakeca-backend/index.php/anuncio/send_notification/?msg=Servico%20Freiniciado')
			time.sleep(5)
		else:
			time.sleep(5)
			init()

	except Exception as ex:
		print('ERRO GENERICO -> ' + str(ex))
		pass

log(' ===== INICIANDO REBOOT ======')


try:
	init()
except Exception as ex:
	print('ERRO GENERICO -> ' + str(ex))

