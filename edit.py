from selenium import webdriver 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from selenium.webdriver.firefox.options import Options
from base64 import decodestring
import base64
import argparse
import time 
import os
import datetime
import psutil
import gc
import sys
import re
import requests
import urllib2
import socket

proxies = {'http': '104.223.115.48:3128', 'https': '104.223.115.48:3128'}

env = socket.getfqdn()

if(env=='dolci5-ny'):
	syspath='/var/www/automacao-bakeka/'

	firefox_profile = webdriver.FirefoxProfile()
	firefox_options = Options()
	firefox_options.headless = False
	myProxy = "104.223.115.48:3128"

	firefox_profile.set_preference("network.proxy.type", 1)
	firefox_profile.set_preference("network.proxy.http", "104.223.115.48")
	firefox_profile.set_preference("network.proxy.http_port", 3128)
	firefox_profile.update_preferences() 

	proxy = "104.223.115.48:3128"

	firefox_capabilities = webdriver.DesiredCapabilities.FIREFOX
	firefox_capabilities['marionette'] = True

	firefox_capabilities['proxy'] = {
		"proxyType": "MANUAL",
		"httpProxy": myProxy,
		"ftpProxy": myProxy,
		"sslProxy": myProxy
	}

	
	driver = webdriver.Firefox(executable_path="/var/tmp/geckodriver", firefox_profile=firefox_profile, options=firefox_options,capabilities=firefox_capabilities)
else:
	os.environ["PATH"] += os.pathsep + r"C:\\chromedriver"
	chrome_profile = webdriver.Chrome()
	chrome_options = Options()
	chrome_options.headless = True
	
	driver = webdriver.Chrome()  # Optional argument, if not specified will search path.

def waitForElement(selector, time):
	WebDriverWait(driver, time).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

def test_proxy():
	try:
		requests.get("https://bakecaincontrii.com/",proxies=proxies)
	except IOError:
		print "Connection error! (Check proxy)"
	else:
		print "All was fine"

def log(acao, tipo = 'LOG', result= 'SUCCESS'):
	try:
		now = datetime.datetime.now()
		now = now.strftime("%d/%m/%Y %H:%M:%S")
		now = str(now)
		gc.collect()
		process = psutil.Process(os.getpid())
		process = process.memory_info().rss/1024
		print(tipo+'['+now+']'+ '-> '+ acao + ' ['+result+']')


	except Exception as ex:
		print(str(ex))
		# raise

def save_log(id_queue="", id_pedido="", log=""):
	url_api = "http://68.183.110.3/bakeca-backend/index.php/anuncio/"
	try:
		requests.post(url_api + '/log', data={'id_queue':id_queue, 'id_pedido':id_pedido, 'log':log},proxies=proxies)
	except Exception as ex:
		print('ERROR -> ' + ex)

def waitElementor(xpath,time):
    element_present = EC.presence_of_element_located((By.CSS_SELECTOR, xpath))
    WebDriverWait(driver, time).until(element_present)


def init(idp = "", indrizzo = "", cap = "", zona = "", titolo = "", testo = "", image_1 = "", image_2 = "", image_3 = "", image_4 = "", image_5 = "", whatsapp=0, eta="", tel="", id_anuncio="", id_annuncio="", crop_position = "", crop_axys = "", nova_imagem="", id_pedido=""):
	#test_proxy()
	url = 'https://varese.bakecaincontrii.com/fe/main.php?page=post_update&ref=post_manage&idp=' + str(idp)
	url_changed = url + '&adding=1'
	images = []
	url_api = "http://68.183.110.3/bakeca-backend/index.php/anuncio/"

	log('IDP -> ' + str(idp))
	save_log(id_anuncio, id_pedido, 'PAGINA DO ANUNCIO CARREGADA')
	crop_position = int(crop_position)
	driver.get('https://milano.bakecaincontrii.com/fe/main.php?page=ctt_choice_ajax&cosa=costo&tipo_ctt=1&fascia_oraria=42&num_giorni=12&codcat=31&vhost=69')
	driver.add_cookie({'name' : 'adult_cookie', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1577750340'})
	driver.add_cookie({'name' : 'no_more_promo_vc', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1577750340'})
	driver.get(url)
	try:
		waitElementor('#accetto', 10)
		log('ANUNCIO CARREGADO')
		try:
			waitElementor('.ins-box-titolo', 5)

			# Declarando Campos
			#log('COLETANDO CAMPOS DO FORM')
			_indrizzo = driver.find_element_by_css_selector('#indirizzo-ins')
			_cap = driver.find_element_by_css_selector('#cap-ins')
			_zona = driver.find_element_by_css_selector('#zona-ins')
			_titolo = driver.find_element_by_css_selector('#titolo-ins')
			_testo = driver.find_element_by_css_selector('#testo-ins')
			_eta = driver.find_element_by_css_selector('#eta-ins')
			_tel = driver.find_element_by_css_selector('#tel-ins')

			# Limpando todos os Campos
			log('LIMPANDO CAMPOS DO FORM')

			if indrizzo:
				_indrizzo.clear()
				_indrizzo.send_keys(indrizzo)

			if cap:
				_cap.clear()
				_cap.send_keys(cap)

			if zona:
				_zona.clear()
				_zona.send_keys(zona)

			if titolo:
				_titolo.clear()
				regex = r"<br\s*[\/]?>"
				titolo = re.sub(regex, '\r\n', titolo)
				titolo = titolo.replace("&#039;", "'")
				_titolo.send_keys(titolo)

			if testo:
				_testo.clear()
				testo = re.sub(regex, '\r\n', testo)
				testo = testo.replace("&#039;", "'")
				_testo.send_keys(testo)

			if eta:
				_eta.clear()
				_eta.send_keys(eta)

			if tel:
				_tel.clear()
				_tel.send_keys(tel)

			# Preenchendo Campos
			log('PREENCHENDO FORM')

			if whatsapp == '1':
				log('COLOCANDO WHATSAPP')
				try:
					chekbox = driver.find_element_by_xpath('//*[@id="whatsapp"]')

					if chekbox.is_selected():
						#log('WHATSAPP JA COLOCADO')
						pass

					else:
						#log('WHATSAPP COLOCADO')
						chekbox.click()

				except Exception as ex:
					pass

			if nova_imagem == '1':
				log('NOVA IMAGEM')
				save_log(id_anuncio, id_pedido, 'UTILIZAR NOVAS IMAGENS')

				if image_1 != "":
					images.append(image_1)

				if image_2 != "":
					images.append(image_2)

				if image_3 != "":
					images.append(image_3)
				
				if image_4 != "":
					images.append(image_4)
				
				if image_5 != "":
					images.append(image_5)
				

				try:
					log('CONTANDO IMAGENS PARA DELETAR')
					elemns = driver.find_elements_by_css_selector("#box-inserimentofoto .icon-delete")
					log('total encontradas '+str(len(elemns)))

					for x in range(1,len(elemns)):
						try:
							log('DELETANDO IMAGENS')
							#driver.find_element_by_css_selector(".icon-delete").click()
							driver.execute_script("$('#box-inserimentofoto .icon-delete').last().click();")
							time.sleep(2)
						except Exception as ex:
							log(str(ex), 'ERRO', 'ERROR')	

					imgs_to_up = ''
					primeiro = 0
					for image in images:
						log('IMAGE -> '+str(image))
						try:
							if str(image) != "None":
								if(primeiro==0):
									imgs_to_up = image
									primeiro = 1
								else:
									imgs_to_up = imgs_to_up+ "\n" +image
						except Exception as ex:
							log(str(ex), 'ERRO', 'ERROR')
							save_log(id_anuncio, id_pedido, 'ERRO  -> '+str(ex))
							requests.get(url_api + '/confirmar/'+str(id_anuncio)+'/5',proxies=proxies)
							requests.get(url_api + '/tentativa/'+str(id_anuncio),proxies=proxies)
							driver.quit()
							sys.exit(1)		
							raise
					try:
						driver.find_element_by_id("upfile").send_keys(imgs_to_up)
						WebDriverWait(driver, 10).until(EC.url_changes(url_changed))
						save_log(id_anuncio, id_pedido, 'IMAGEMS ENVIADAS')
					except Exception as ex:
						log('ERRO 5a  ->' + str(ex))
						driver.quit()
						sys.exit(1)		
						raise
					try:
						driver.find_element_by_id("img_crop_upload").send_keys(images[crop_position])
						WebDriverWait(driver, 10).until(EC.url_changes(url_changed))
						save_log(id_anuncio, id_pedido, 'IMAGEM DE CAPA ENVIADA')
					except Exception as ex:
						log('ERRO 5b  ->' + str(ex))
						requests.get(url_api + '/tentativa/'+str(id_anuncio),proxies=proxies)
						save_log(id_anuncio, id_pedido, "ERRORE: Hai selezionato l'immagine "+str(crop_position)+" ma ce ne sono solo "+str(len(images))+" nella richiesta. Controlla il tuo annuncio.")
						driver.quit()
						sys.exit(1)		
						raise

				except Exception as ex:
					# log('ERRO 4  ->' + str(ex))
					pass
			elif nova_imagem is None and crop_position>=0:
				crop_position = crop_position+1
				nova_thumb_src = driver.find_element_by_css_selector("#image-upload-container-boxes .item-draggable:nth-child("+str(crop_position)+") .img-preview-container  #img-preview-crop").get_attribute('src')
				nova_thumb_src = nova_thumb_src.split('?')[0]
				tmp_file = syspath+str(time.time())+'.jpg'
				log(tmp_file)
				with open(tmp_file,'wb') as f:
					f.write(urllib2.urlopen('http://68.183.110.3/thumbs2.php?imagem='+nova_thumb_src).read())
					f.close()
				driver.execute_script("$('#img-preview-container-first .group-container .icons-container-placeholder .icon-delete').click();")
				driver.find_element_by_id("img_crop_upload").send_keys(tmp_file)
				WebDriverWait(driver, 10).until(EC.url_changes(url_changed))
				save_log(id_anuncio, id_pedido,'NOVA IMAGEM DE CAPA ENVIADA')
				os.remove(tmp_file)
				#sys.exit(1)	
			#else:
				#log('nova_imagem:'+str(nova_imagem))
				#log('crop_position:'+str(crop_position))
				#sys.exit(1)	

			if crop_position:
				log('CROP IMAGE POSITIONS -> ' + crop_axys)
				try:
					crop_axys = crop_axys.split(",")
					driver.execute_script('document.querySelector("input[name=preview_x]").value='+str(crop_axys[0]))
					driver.execute_script('document.querySelector("input[name=preview_y]").value='+str(crop_axys[1]))
					driver.execute_script('document.querySelector("input[name=preview_width]").value='+str(crop_axys[2]))
					driver.execute_script('document.querySelector("input[name=preview_height]").value='+str(crop_axys[3]))

					driver.execute_script('document.querySelector("input[name=to_recut]").value=1')
					driver.execute_script('document.querySelector("input[name=preview]").value=1')
					driver.execute_script('document.querySelector("input[name=image_replaced]").value=1')

					save_log(id_anuncio, id_pedido, 'CROP REALIZADO COM EXITO')

				except Exception as ex:
					log('ERRO 6 ' + str(ex))
					save_log(id_anuncio, id_pedido, 'ERRORE: ERRORE DI TAGLIO ANTEPRIMA / '+str(ex))
					requests.get(url_api + '/tentativa/'+str(id_anuncio),proxies=proxies)
					driver.quit()
					sys.exit(1)
					raise	

			try:
				time.sleep(2)
				driver.find_element_by_css_selector('#submit-ins').click()
				#waitElementor('.lb-contenitores',5000)

				try:
					waitElementor('.lb-contenitore > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > p:nth-child(1)', 60)

					try:
						driver.find_element_by_css_selector('body > div.lb-contenitore > div > div > div.lb-contenuto.form-default > div:nth-child(2) > div > button').click()

						try:
							waitForElement('#immagine-anteprima-ctt > img', 10)
							image_thumb = driver.find_element_by_css_selector('#immagine-anteprima-ctt > img').get_attribute('src')

							b64_thumb = base64.b64encode(requests.get(image_thumb).content,proxies=proxies)
							#log('THUMB -> BASE64')
							
							log('ANUNCIO MODIFICADO COM SUCESSO')
							save_log(id_anuncio, id_pedido, 'ANUNCIO MODIFICADO COM SUCESSO')
							requests.post(url_api + '/atualizar_link/' + id_annuncio,proxies=proxies)
							requests.post(url_api + '/atualizar_thumb/' + str(idp), data={'img':b64_thumb},proxies=proxies)
							requests.get(url_api + '/confirmar/'+str(id_anuncio)+'/1',proxies=proxies)

						except Exception as ex:
							log(str(ex), 'ERRO', 'ERROR')
							save_log(id_anuncio, id_pedido, 'ERRORE: ERRORE PER OTTENERE NUVOA ANTEPRIMAA / '+str(ex))
							driver.quit()
							sys.exit(1)

					except Exception as ex:
						log(str(ex), 'ERRO', 'ERROR')
						save_log(id_anuncio, id_pedido, 'ERRO  -> '+str(ex))
						driver.quit()
						sys.exit(1)

				except Exception as ex:
					log('PROBLEMA AO MODIFICAR O ANUNCIO ->' + str(ex))
					save_log(id_anuncio, id_pedido, 'ERRO  -> '+str(ex))
					requests.get(url_api + '/tentativa/'+str(id_anuncio),proxies=proxies)
					driver.quit()
					sys.exit(1)
					raise

			except Exception as ex:
				pass
				# log('ERRO 4  ->' + str(ex))		

		except Exception as ex:
			log(str(ex), 'ERRO', 'ERROR')
			save_log(id_anuncio, id_pedido, 'ERRORE:  ANUNCIO NON TROVATO / -> '+str(ex))
			requests.get(url_api + '/tentativa/'+str(id_anuncio),proxies=proxies)
			driver.quit()
			sys.exit(1)					

	except Exception as ex:
		pass
		log(str(ex), 'ERRO', 'ERROR')
		save_log(id_anuncio, id_pedido, 'ERRO  -> '+str(ex))
		requests.get(url_api + '/tentativa/'+str(id_anuncio),proxies=proxies)
		driver.quit()
		sys.exit(1)	


log('INICIANDO')
parser = argparse.ArgumentParser()
parser.add_argument('--idp', required=True)
parser.add_argument('--indirizzo', required=False)
parser.add_argument('--cap', required=False)
parser.add_argument('--zona', required=False)
parser.add_argument('--titolo', required=False)
parser.add_argument('--testo', required=False)
parser.add_argument('--foto1', required=False)
parser.add_argument('--foto2', required=False)
parser.add_argument('--foto3', required=False)
parser.add_argument('--foto4', required=False)
parser.add_argument('--foto5', required=False)
parser.add_argument('--whatsapp', required=False)
parser.add_argument('--eta', required=False)
parser.add_argument('--tel', required=False)
parser.add_argument('--id', required=True)
parser.add_argument('--id_annuncio', required=True)
parser.add_argument('--crop_img_pos', required=False)
parser.add_argument('--crop_img_axys', required=False)
parser.add_argument('--nova_imagem', required=False)
parser.add_argument('--id_pedido', required=False)

args = parser.parse_args()

# ===========================================================================================
# COMO UTILIZAR
# utilizar atributos no shell como acima, ex: python run.py -idp=394023948203948 -indrizzo="EOFNPE WEOFNWPEOFN" ....


init(args.idp, args.indirizzo, args.cap, args.zona, args.titolo, args.testo, args.foto1, args.foto2, args.foto3, args.foto4, args.foto5, args.whatsapp, args.eta, args.tel, args.id, args.id_annuncio, args.crop_img_pos, args.crop_img_axys, args.nova_imagem, args.id_pedido)

driver.quit()
