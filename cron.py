import urllib
from subprocess import Popen
import sys
from base64 import decodestring
import base64
import time 
import requests
import os
import datetime
import logging
import gc
import psutil
import re
import glob
import socket

env = socket.getfqdn()
proxies = {'http': '185.30.232.25:20001', 'https': '185.30.232.25:20001'}

url_api = "http://localhost/bakeca-backend/index.php/anuncio"

if(env=='dolci5-ny'):
	syspath='/var/www/automacao-bakeka/'
	datapath = '/var/www/dolci/' 
elif(env=='videochat.dolcincontri.com'):
	syspath='/home/pannello/automacao-bakeka/'
	datapath = syspath
	
def send_notification(id_agente, msg):
	try:
		requests.post(url_api+'/send_notification/',  data={'id_agente':id_agente,'msg':msg})
	except Exception as ex:
		save_log('ERROR -> ' + ex)
		print('ERROR -> ' + ex)

def save_log(id_queue="", id_pedido="", log=""):
	try:
		requests.post(url_api + '/log', data={'id_queue':id_queue, 'id_pedido':id_pedido, 'log':log})
	except Exception as ex:
		print('ERROR -> ' + ex)

def str_clean(str):
	return re.sub('[^A-Za-z0-9]+', '', str)


def log(acao, tipo = 'LOG'):
	try:
		now = datetime.datetime.now()
		now = now.strftime("%d/%m/%Y %H:%M:%S")
		now = str(now)
		gc.collect()
		process = psutil.Process(os.getpid())
		process = process.memory_info().rss/1024

		print(tipo+'['+now+']'+ '-> '+ acao)
	except Exception as ex:
		log(str(ex))

def init():
	#log('ENTRANDO NA API')
	try:
		retorno = requests.get(url_api+'/get').json()
		#log('REQUEST -> ' + str(retorno['result']))
		if(retorno['result'] == True):
			acao = str(retorno['data']['acao'])
			log('REQUEST -> ' + acao)
			try:
				if (acao == 'suspender'):
					#SUSPENDER
					log('NOVA ENTRADA NA API: SUSPENDER')
					dados = retorno['data']
					dados_anuncio = retorno['data']['annuncio_dati']
					command = syspath+'suspende.py --idp="'+dados_anuncio['idp']+'" --id='+str(dados['id'])+' --id_annuncio='+dados['id_annuncio']
					save_log(dados['id'], dados['id_pedido'], command)
					log('COMMAND -> ' + command)
					p = Popen("python " + command, shell=True)
					p.wait()
					init()
				
				if (acao == 'pagar'):
					#PAGAR
					log('NOVA ENTRADA NA API: PAGAR')
					session_data = requests.get(url_api+'/get_session/2').json()
					if(session_data['result']==True):
						dados_anuncio = retorno['data']
						command = syspath+'pay.py --phpsessid="'+session_data['phpsession']+'" --idp="'+dados_anuncio['url']+'" --id='+dados_anuncio['id']+' --id_agente='+dados_anuncio['id_bakeka_agenti']+'  --id_pedido="'+dados_anuncio['id_bakeka_pedido']+'"'
						command = command + '--idp="'+dados_anuncio['url']+'" --product='+dados_anuncio['product']+'  --horario="'+dados_anuncio['horario']+'"  --horario="'+dados_anuncio['horario']+'"'
						command = command + '  --data_inserido="'+dados_anuncio['data_inserido']+'"  --custo_euro="'+dados_anuncio['custo_euro']+'"'
						command = command + '  --bkk_credits="'+dados_anuncio['bkk_credits']+'"  --dolci_credits="'+dados_anuncio['dolci_credits']+'"  --id_agenti='+dados_anuncio['id_agenti']
						command = command + '  --username="'+dados_anuncio['username']+'" --email="'+dados_anuncio['status']+'"  --statusd='+dados_anuncio['status']
						save_log(dados_anuncio['id'], dados_anuncio['id_bakeka_pedido'], command)
						p = Popen("python " + command, shell=True)
						p.wait()
						init()

				elif (acao == 'editar'):
					send_notification(2, 'NOVA ENTRADA NA API: EDITAR')
					#MODIFICAR
					log('NOVA ENTRADA NA API: EDITAR')

					dados = retorno['data']
					dados_anuncio = retorno['data']['annuncio_dati']

					command = syspath+'edit.py --idp="'+dados_anuncio['idp']+'" --id='+str(dados['id'])+' --id_annuncio='+dados['id_annuncio']

					if (dados['titolo']):
						titolo = dados['titolo'].replace('`', "'")
						command = command + ' --titolo="'+titolo+'"'

					if (dados['testo']):
						testo = dados['testo'].replace('`', "'")
						command = command + ' --testo="'+testo+'"'

					if (dados_anuncio['indirizzo_ins']):
						command = command + ' --indirizzo="'+dados_anuncio['indirizzo_ins']+'"'

					if (dados_anuncio['zona_ins']):
						command = command + ' --zona="'+str(dados_anuncio['zona_ins'])+'"'

					if (dados_anuncio['cap_ins']):
						command = command + ' --cap="'+str(dados_anuncio['cap_ins'])+'"'

					if (dados_anuncio['eta_ins']):
						command = command + ' --eta="'+str(dados_anuncio['eta_ins'])+'"'

					if (dados_anuncio['whatsapp'] == 'on'):
						command = command + ' --whatsapp=1'

					if (dados_anuncio['telefono_ins']):
						command = command + ' --tel="'+str(dados_anuncio['telefono_ins'])+'"'
					
					if (dados['id_pedido']):
						command = command + ' --id_pedido="'+str(dados['id_pedido'])+'"'

					if (dados_anuncio['img_selected']):
						command = command + ' --crop_img_pos='+dados_anuncio['img_selected'][0]
						command = command + ' --crop_img_axys="'+str(dados_anuncio['crop'][0])+'"'
					
					if (dados_anuncio['nova_imagem']=="1"):
						command = command + ' --nova_imagem="'+dados_anuncio['nova_imagem']+'"'
						# images = dados_anuncio['preloaded_images'].split(',')
						folder_name = datapath+str(retorno['data']['id']) + '/'

						log('FOLDER -> ' + str(folder_name))

						if not os.path.exists(folder_name):
							log('PASTA NAO EXISTE EXIT PROCCESS')
							save_log(dados['id'], dados['id_pedido'], 'PASTA NAO EXISTE')
							requests.get(url_api + '/confirmar/'+str(dados['id'])+'/9')
							exit()

						else:
							log('PASTA ENCONTRADA')
							os.chdir(folder_name)
							val = 0
							for file in sorted(glob.glob("*.jpg")):
								log('FILE -> ' + str(file))
								val = val + 1
								if(val<=5): 
									command = command + ' --foto'+str(val)+'="'+folder_name+str(file)+'"'
								
					else:
						log('SEM NOVAS IMAGEM '+dados_anuncio['nova_imagem'])		


					save_log(dados['id'], dados['id_pedido'], command)
					log('COMMAND -> ' + command)
					p = Popen("python " + command, shell=True)
					p.wait()
					init()

				#INSERIR
				elif (acao == 'inserir'):
					log('NOVA ENTRADA NA API: INSERIR')
					#send_notification(2, 'NOVA ENTRADA NA API: INSERIR')

					dados = retorno['data']
					dados_anuncio = retorno['data']['annuncio_dati']

					log('img_selected -> ' + dados_anuncio['img_selected'])

					url_get_session = url_api+"/get_session/"+str(dados['id_agente'])
					session_data = requests.get(url_get_session).json()
					log('session:'+str(session_data['phpsession']))
					log('id_agente:'+str(dados['id_agente']))
					if (str(dados['id_agente'])=='2'):
						command = syspath+'insert_test.py --phpsessid="'+str(session_data['phpsession'])+'" --id_fila='+str(dados['id'])+' --id_annuncio='+dados['id_annuncio']
					else:
						command = syspath+'insert.py --phpsessid="'+str(session_data['phpsession'])+'" --id_fila='+str(dados['id'])+' --id_annuncio='+dados['id_annuncio']

					#log('cidade:'+dados_anuncio['citta_ins'])
				
					if (dados_anuncio['citta_ins']):
						command = command + ' --citta="'+dados_anuncio['citta_ins']+'"'
					
					if (dados_anuncio['categoria_ins']):
						command = command + ' --categoria="'+dados_anuncio['categoria_ins']+'"'

					if (dados['titolo']):
						command = command + ' --titolo="'+dados['titolo']+'"'

					if (dados['id_pedido']):
						command = command + ' --id_pedido="'+dados['id_pedido']+'"'
					
					if (dados['id_agente']):
						command = command + ' --id_agente="'+dados['id_agente']+'"'

					if (dados['testo']):
						command = command + ' --testo="'+dados['testo']+'"'

					if (dados_anuncio['indirizzo_ins']):
						command = command + ' --indirizzo="'+dados_anuncio['indirizzo_ins']+'"'

					if (dados_anuncio['zona_ins']):
						command = command + ' --zona="'+str(dados_anuncio['zona_ins'])+'"'

					if (dados_anuncio['cap_ins']):
						command = command + ' --cap="'+str(dados_anuncio['cap_ins'])+'"'

					if (dados_anuncio['eta_ins']):
						command = command + ' --eta="'+str(dados_anuncio['eta_ins'])+'"'

					if (dados_anuncio['whatsapp'] == 'on'):
						command = command + ' --whatsapp=1'

					if (dados_anuncio['tipo_toplist']):
						command = command + ' --tipo_ctt="'+str(dados_anuncio['tipo_toplist'])+'"'
					
					if (dados_anuncio['giorni']):
						command = command + ' --giorni_select="'+str(dados_anuncio['giorni'])+'"'
					
					if (dados_anuncio['orario']):
						command = command + ' --fascia_selected="'+str(dados_anuncio['orario'])+'"'
					
					if (dados_anuncio['telefono_ins']):
						command = command + ' --tel="'+str(dados_anuncio['telefono_ins'])+'"'


					if (dados_anuncio['img_selected']):
						command = command + ' --crop_img_pos='+dados_anuncio['img_selected']
						command = command + ' --crop_img_axys="'+str(dados_anuncio['crop'])+'"'

					# images = dados_anuncio['preloaded_images'].split(',')
					if(env=='dolci5-ny'):
						folder_name = datapath+str(retorno['data']['id_pedido']) + '/' + str(retorno['data']['cloni'])+'/'

						log('FOLDER -> ' + str(folder_name))

						if not os.path.exists(folder_name):
							log('PASTA NAO EXISTE EXIT PROCCESS')
							save_log(dados['id'], dados['id_pedido'], 'PASTA NAO EXISTE')
							requests.get(url_api + '/confirmar/'+str(dados['id'])+'/9')

							exit()

						else:
							log('PASTA ENCONTRADA')
							os.chdir(folder_name)
							val = 0
							for file in sorted(glob.glob("*.jpg")):
								log('FILE -> ' + str(file))
								val = val + 1
								command = command + ' --foto'+str(val)+'="'+folder_name+str(file)+'"'
					elif(env=='videochat.dolcincontri.com'):
						log('USANDO CLOUDINARY')
						val = 1
						for file in dados_anuncio['files']:
							log('FILE -> ' + str(file))
							command = command + ' --foto'+str(val)+'="'+str(file)+'"'
							val = val + 1
							
							


					save_log(dados['id'], dados['id_pedido'], command)
					log('COMMAND -> ' + command)
					p = Popen("python " + command, shell=True)
					p.wait()
					init()



			except Exception as ex:
				msg = "ERRO ANUNCIO: Fila: "+str(dados['id'])+" Pedido:"+str(dados['id_pedido'])

				send_notification(2, msg)
				print('ERRO -> ' + str(ex))
				pass
		else:
			now = datetime.datetime.now()
			now = now.strftime("%-M")
			min=int(now)
			umavez = 0
			if(((min%5)==0) and (umavez==0)):
				log('SEM ANUNCIOS NA FILA')
				umavez = 1
			else:
				umavez = 0
			time.sleep(5)
			init()

	except Exception as ex:
		print('ERRO GENERICO -> ' + str(ex))
		pass

log(' ===== INICIANDO APLICACAO ======')


def isAlive():
	myPID = str(os.getpid())
	result = os.popen("/bin/pidof python cron.py").read()
	pids = result.split(" ")
	active = False
	for pid in pids:
		pid = pid.replace("\n","")
		log("Comparando PIDs '" + pid + "' x '" + myPID + "'")
		if pid != myPID: 
			active = True

	if active:
		log("CRON esta ativo...")
		return True
	else:
		log("CRON esta inativo...")
		return False

def start():
	log("CRON: Verificando existencia do processo!")
	if not isAlive():
		init()


try:
	start()
except Exception as ex:
	print('ERRO GENERICO -> ' + str(ex))

