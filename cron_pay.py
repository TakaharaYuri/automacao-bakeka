import urllib
from subprocess import Popen
import sys
from base64 import decodestring
import base64
import time 
import requests
import os
import datetime
import logging
import gc
import psutil
import re
import glob
import socket

proxies = {'http': '104.223.115.48:3128', 'https': '104.223.115.48:3128'}
url_api = "http://localhost/bakeca-backend/index.php/anuncio"

env = socket.getfqdn()

if(env=='dolci5-ny'):
	syspath='/var/www/automacao-bakeka/'
	datapath = '/var/www/dolci/'
else:
	syspath='C:\\wamp64\\www\\automacao-bakeka\\'
	datapath = syspath

def save_log(id_queue="", id_pedido="", log=""):
	try:
		requests.post(url_api+'/log', data={'id_queue':id_queue, 'id_pedido':id_pedido, 'log':log})
	except Exception as ex:
		print('ERROR -> ' + ex)

def str_clean(str):
	return re.sub('[^A-Za-z0-9]+', '', str)


def log(acao, tipo = 'LOG'):
	try:
		now = datetime.datetime.now()
		now = now.strftime("%d/%m/%Y %H:%M:%S")
		now = str(now)
		gc.collect()
		process = psutil.Process(os.getpid())
		process = process.memory_info().rss/1024

		print(tipo+'['+now+']'+ '-> '+ acao)
	except Exception as ex:
		log(str(ex))

def init():
	try:
		retorno = requests.get(url_api+'/get_pagar').json()
		if(retorno['result'] == True):
			try:
				log('NOVA ENTRADA NA API: PAGAR')
				session_data = requests.get(url_api+'/get_session/2').json()
				if(session_data['result']==True):
					dados_anuncio = retorno['dados']
					command = syspath+'pay.py --phpsessid="'+session_data['phpsession']+'" --idp="'+dados_anuncio['url']+'" --id='+dados_anuncio['id']+' --id_agente='+dados_anuncio['id_bakeka_agenti']+'  --id_pedido="'+dados_anuncio['id_bakeka_pedido']+'"'
					command = command + '--idp="'+dados_anuncio['url']+'" --product='+dados_anuncio['product']+'  --horario="'+dados_anuncio['horario']+'"  --horario="'+dados_anuncio['horario']+'"'
					command = command + '  --data_inserido="'+dados_anuncio['data_inserido']+'"  --custo_euro="'+dados_anuncio['custo_euro']+'"'
					command = command + '  --bkk_credits="'+dados_anuncio['bkk_credits']+'"  --dolci_credits="'+dados_anuncio['dolci_credits']+'"  --id_agenti='+dados_anuncio['id_agenti']
					command = command + '  --username="'+dados_anuncio['username']+'" --email="'+dados_anuncio['email']+'"  --status='+dados_anuncio['status']
					#save_log(dados_anuncio['id'], dados_anuncio['id_bakeka_pedido'], command)
					p = Popen("python " + command, shell=True)
					p.wait()
					init()
				else:
					print('ERRO SESSAO')
					pass
			except Exception as ex:
				print('ERRO -> ' + str(ex))
				pass
		else:
			now = datetime.datetime.now()
			now = now.strftime("%-M")
			min=int(now)
			if((min%10)==0):
				log('SEM ANUNCIOS PARA PAGAR')
			time.sleep(5)
			init()

	except Exception as ex:
		print('ERRO GENERICO -> ' + str(ex))
		pass

log(' ===== INICIANDO APLICACAO ======')


def isAlive():
	myPID = str(os.getpid())
	result = os.popen("python cron_pay.py").read()
	pids = result.split(" ")
	print(pids)
	active = False
	for pid in pids:
		pid = pid.replace("\n","")
		log("Comparando PIDs '" + pid + "' x '" + myPID + "'")
		if pid != myPID: 
			active = True

	if active:
		log("CRON esta ativo...")
		return True
	else:
		log("CRON esta inativo...")
		return False

def start():
	log("CRON: Verificando existencia do processo!")
	if not isAlive():
		init()


try:
	init()
except Exception as ex:
	print('ERRO GENERICO -> ' + str(ex))
	input('erro....')

