from selenium import webdriver 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC 
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.proxy import Proxy, ProxyType
from base64 import decodestring

import base64
import argparse
import time 
import os
import datetime
import psutil
import gc
import sys
import re
import requests
import urllib
import socket
proxies = {'http': '104.223.115.48:3128', 'https': '104.223.115.48:3128'}
url_api = "http://localhost/bakeca-backend/index.php/anuncio/"

env = socket.getfqdn()

if(env=='dolci5-ny'):
	firefox_profile = webdriver.FirefoxProfile()
	firefox_options = Options()
	firefox_options.headless = False
	myProxy = "104.223.115.48:3128"
	firefox_profile.set_preference('permissions.default.image', 2)
	firefox_profile.update_preferences() 

	firefox_capabilities = webdriver.DesiredCapabilities.FIREFOX
	firefox_capabilities['marionette'] = True

	firefox_capabilities['proxy'] = {
		"proxyType": "MANUAL",
		"httpProxy": myProxy,
		"ftpProxy": myProxy,
		"sslProxy": myProxy
	}

	
	driver = webdriver.Firefox(executable_path="/var/tmp/geckodriver", firefox_profile=firefox_profile, options=firefox_options,capabilities=firefox_capabilities)
else:
	os.environ["PATH"] += os.pathsep + r"C:\\chromedriver"
	chrome_profile = webdriver.Chrome()
	chrome_options = Options()
	chrome_options.headless = True
	
	driver = webdriver.Chrome()  # Optional argument, if not specified will search path.

def waitForElement(selector, time):
	WebDriverWait(driver, time).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

def elementIsClicable(selector, time):
	WebDriverWait(driver, time).until(EC.element_to_be_clickable((By.CSS_SELECTOR,selector)))

def log(acao, tipo = 'LOG', result= 'SUCCESS'):
	try:
		now = datetime.datetime.now()
		now = now.strftime("%d/%m/%Y %H:%M:%S")
		now = str(now)
		gc.collect()
		process = psutil.Process(os.getpid())
		process = process.memory_info().rss/1024
		print(tipo+'['+now+']'+ '-> '+ acao + ' ['+result+']')


	except Exception as ex:
		print(str(ex))
		# raise

def save_log(id_queue="", id_pedido="", log=""):
	try:
		requests.post(url_api + '/log', data={'id_queue':id_queue, 'id_pedido':id_pedido, 'log':log})
	except Exception as ex:
		print('ERROR -> ' + ex)

def send_notification(id_agente, msg):
	try:
		requests.post(url_api+'/send_notification/',  data={'id_agente':id_agente,'msg':msg})
	except Exception as ex:
		save_log('ERROR -> ' + ex)
		print('ERROR -> ' + ex)


def waitElementor(xpath,time):
    element_present = EC.presence_of_element_located((By.CSS_SELECTOR, xpath))
    WebDriverWait(driver, time).until(element_present)


def init(id="", phpsessid="", idp = "", id_agente = "", id_pedido = "", product = "", horario = "", data_inserido = "", custo_euro = "", bkk_credits = "", dolci_credits = "", id_agenti = "", username= "", email="", statusd=""):
        log(email)
	if(email=="6"):
		#RENOVAR
		url = 'https://www.bakecaincontrii.com/fe/main.php?page=ctt_do_renew&idp='+idp+'&renew=1'
	else:
		#PAGAR
		url = 'https://www.bakecaincontrii.com/fe/main.php?page=ctt_pay&idp='+idp
	log('IDP -> ' + str(url))
	
	driver.get('https://www.bakecaincontrii.com/fe/main.php?page=ctt_choice_ajax&cosa=costo&tipo_ctt=1&fascia_oraria=42&num_giorni=12&codcat=31&vhost=69')
	driver.add_cookie({'name' : 'adult_cookie', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
	driver.add_cookie({'name' : 'privacy_cookie', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
	driver.add_cookie({'name' : 'PHPSESSID', 'value' : phpsessid, 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
	driver.add_cookie({'name' : 'no_more_push_premium', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
	driver.add_cookie({'name' : 'no_more_promo_vc', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})

	driver.get(url)
	try: 
		driver.find_element_by_css_selector('#promo-premium-actions a').click()
	except:
		pass

	try:
		time.sleep(2)
		waitElementor('#accetto', 5)
		driver.find_element_by_xpath('//*[@id="accetto"]').click()
		log("TEM POPUP")
	except:
		log("FALHA NO POPUP")
		pass
		
	try:
		publicado = driver.find_element_by_css_selector('.content-principale .bottone').text
		if(publicado.find("PROCEDI")!=-1):
			requests.get(url_api+'/update_anuncio/'+str(id)+'/'+str(idp)+'/1')
			log('ANUNCIO JA PUBLICADO')
			driver.quit()
			sys.exit(1)
	except:
		log('EXISTE')
		pass

	# try:
	# 	mensagem_promo = driver.find_element_by_css_selector('.bottone-video-50')
	# 	if(mensagem_promo):
	# 		log('MENSAGEM PROMO')
	# 		mensagem_promo.click()
	# except:
	# 	log('EXISTE')
	# 	pass	
	
	try:
		msg = ""
		bkk_credits = int(bkk_credits)
		#waitElementor('.post-manage-section-title',10)
		#waitElementor('#promuovi', 10)
		#time.sleep(2)
		log('ANUNCIO CARREGADO')
		#PROMOVER
		#driver.find_element_by_css_selector('#promuovi').click()
		waitElementor('#acquista', 10)
		#VERIFICA VALOR
		costo_credito = driver.find_elements_by_css_selector('td[data-title="Prezzo:"]')[0].text
		costo_credito = re.search(r'\((.*?)\)',costo_credito).group(1).replace(' CREDITI','')

		try:
			costo_premium = driver.find_elements_by_css_selector('td[data-title="Prezzo:"]')[1].text
			costo_premium = re.search(r'\((.*?)\)',costo_premium).group(1).replace(' CREDITI','')
			log("PREMIUM")
			costo_credito = int(costo_credito)+int(costo_premium)
		except Exception as ex:
			log("NORMAL")
			pass
		log('costo_credito:'+str(costo_credito))
		log('bkk_credits:'+str(bkk_credits))
		if(int(costo_credito)>int(bkk_credits)):
			msg = str(bkk_credits)+" custou:"+str(costo_credito)
			send_notification(2, "ERRO DE DIFERENCA DE CREDITOS bkk:" + id + "," + id_pedido+" -> "+msg)
			log(msg)
		time.sleep(5)	
		driver.find_element_by_css_selector('#acquista').click()
		waitElementor('input.bottone.half', 10)
		crediti_rimanenti = driver.find_element_by_css_selector('.carrello-tabella .last').text
		print("crediti rimanenti"+str(crediti_rimanenti))
		
		driver.find_element_by_css_selector('input.bottone.half').click()
		waitElementor('div.ins-box-titolo h2', 10)
		messaggio = driver.find_element_by_css_selector('div.ins-messaggio p').text
		calendario = driver.find_element_by_css_selector('div.ins-box-titolo h2').text 
		if( messaggio.find('pubblicato') and calendario.find('Calendario') ) :
			requests.get(url_api + '/update_anuncio/'+str(id)+'/'+str(idp)+'/1')
			requests.get(url_api + '/update_credits/'+str(crediti_rimanenti))
			log('ANUNCIO PAGO E PUBLICADO')
		else:
			requests.get(url_api + '/update_anuncio/'+str(id)+'/'+str(idp)+'/4')
			send_notification(2, "ERRO NO PAGAMENTO:" + id + " - " + str(idp) +" - https://arezzo.bakecaincontrii.com/fe/main.php?page=post_manage&idp="+str(idp))
			log(msg)
	except Exception as ex:
		log(str(ex), 'ERRO', 'ANUNCIO INEXISTENTE')
		requests.get(url_api + '/update_anuncio/'+str(id)+'/'+str(idp)+'/4')
		save_log(id, id_pedido, 'ERRO GENERICO -> '+str(ex))
		send_notification(2, "VERIFICAR ANUNCIO:" + id + " - " + str(idp) +" - https://arezzo.bakecaincontrii.com/fe/main.php?page=post_manage&idp="+str(idp)+'\n\n --PAGAR: http://68.183.110.3/bakeca-backend/anuncio/update_anuncio/'+str(id)+'/'+str(idp)+'/0  \n\n --SETAR COMO PAGO: http://68.183.110.3/bakeca-backend/anuncio/update_anuncio/'+str(id)+'/'+str(idp)+'/1')
		driver.quit()
		sys.exit(1)	

#METODOS PARA CRIAR
#atualizar_link/id_link/status
#reembolsar/id_agente/id_link/id_pedido
#get_consumo/idp/id_pedido
#grava_consumo/id_agente/bkk_credits/dolci_credits/id_pedido
#suspender_agente/id_agente/idp
#get_custos_agente/id_agente

log('INICIANDO')
parser = argparse.ArgumentParser()
parser.add_argument('--id', required=True)
parser.add_argument('--idp', required=True)
parser.add_argument('--phpsessid', required=True)
parser.add_argument('--id_agenti', required=True)
parser.add_argument('--id_pedido', required=True)
parser.add_argument('--product', required=True)
parser.add_argument('--horario', required=True)
parser.add_argument('--data_inserido', required=True)
parser.add_argument('--custo_euro', required=True)
parser.add_argument('--bkk_credits', required=True)
parser.add_argument('--dolci_credits', required=True)
parser.add_argument('--id_agente', required=True)
parser.add_argument('--username', required=True)
parser.add_argument('--email', required=True)
parser.add_argument('--statusd', required=True)
args = parser.parse_args()

# ===========================================================================================
# COMO UTILIZAR
# utilizar atributos no shell como acima, ex: python run.py -idp=394023948203948 -indrizzo="EOFNPE WEOFNWPEOFN" ....
try:
	init(args.id, args.phpsessid, args.idp,  args.id_agente, args.id_pedido, args.product, args.horario, args.data_inserido,args.custo_euro, args.bkk_credits, args.dolci_credits, args.username, args.email, args.statusd)
except Exception as ex:
	log(ex)

driver.quit()