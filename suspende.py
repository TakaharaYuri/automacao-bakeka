from selenium import webdriver 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from selenium.webdriver.firefox.options import Options
from base64 import decodestring
import base64
import argparse
import time 
import os
import datetime
import psutil
import gc
import sys
import re
import requests
import urllib2
import socket
env = socket.getfqdn()
proxies = {'http': '104.223.115.48:3128', 'https': '104.223.115.48:3128'}

if(env=='dolci5-ny'):
	firefox_profile = webdriver.FirefoxProfile()
	firefox_options = Options()
	firefox_options.headless = False
	myProxy = "104.223.115.48:3128"
	firefox_profile.set_preference('permissions.default.image', 2)
	firefox_profile.set_preference("javascript.enabled", False)
	firefox_profile.set_preference("network.proxy.type", 1)
	firefox_profile.set_preference("network.proxy.http", "104.223.115.48")
	firefox_profile.set_preference("network.proxy.http_port", 3128)
	firefox_profile.update_preferences() 

	proxy = "104.223.115.48:3128"

	firefox_capabilities = webdriver.DesiredCapabilities.FIREFOX
	firefox_capabilities['marionette'] = True

	firefox_capabilities['proxy'] = {
		"proxyType": "MANUAL",
		"httpProxy": myProxy,
		"ftpProxy": myProxy,
		"sslProxy": myProxy
	}

	
	driver = webdriver.Firefox(executable_path="/var/tmp/geckodriver", firefox_profile=firefox_profile, options=firefox_options,capabilities=firefox_capabilities)
else:
	os.environ["PATH"] += os.pathsep + r"C:\\chromedriver"
	chrome_profile = webdriver.Chrome()
	chrome_options = Options()
	chrome_options.headless = True
	
	driver = webdriver.Chrome()  # Optional argument, if not specified will search path.


def waitForElement(selector, time):
	WebDriverWait(driver, time).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

def log(acao, tipo = 'LOG', result= 'SUCCESS'):
	try:
		now = datetime.datetime.now()
		now = now.strftime("%d/%m/%Y %H:%M:%S")
		now = str(now)
		gc.collect()
		process = psutil.Process(os.getpid())
		process = process.memory_info().rss/1024
		print(tipo+'['+now+']'+ '-> '+ acao + ' ['+result+']')
	except Exception as ex:
		print(str(ex))
		# raise
def send_notification(id_agente, msg):
	try:
		requests.post('http://68.183.110.3/bakeca-backend/index.php/anuncio/send_notification/',  data={'id_agente':id_agente,'msg':msg},proxies=proxies)
	except Exception as ex:
		save_log('ERROR -> ' + ex)
		print('ERROR -> ' + ex)

def save_log(id_queue="", log=""):
	url_api = "http://68.183.110.3/bakeca-backend/index.php/anuncio/"
	try:
		requests.post(url_api + '/log', data={'id_queue':id_queue, 'id_pedido':0, 'log':log},proxies=proxies)
	except Exception as ex:
		print('ERROR -> ' + ex)

def waitElementor(xpath,time):
    element_present = EC.presence_of_element_located((By.CSS_SELECTOR, xpath))
    WebDriverWait(driver, time).until(element_present)


def init(idp = "",id_anuncio="", id_annuncio=""):
	url = 'https://bakecaincontrii.com/fe/main.php?page=post_manage&idp=' + str(idp)
	url_api = "http://68.183.110.3/bakeca-backend/index.php/anuncio/"
	driver.get('https://varese.bakecaincontrii.com/null/')
	driver.add_cookie({'name' : 'adult_cookie', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
	driver.add_cookie({'name' : 'privacy_cookie', 'value' : '1', 'domain' : '.bakecaincontrii.com','expires' : '1609286340'})
	log('IDP -> ' + str(idp))
	driver.get(url)
	log(id_anuncio, 'PAGINA DO ANUNCIO CARREGADA')
	try:
		time.sleep(2)
		waitElementor('#accetto', 5)
		driver.find_element_by_xpath('//*[@id="accetto"]').click()
	except:
		log("FALHA NO POPUP")
		pass
	try:
		waitElementor('.js_suspend', 2)
		try:
			log('PAGINA CARREGADA COM SUCESSO')
			driver.find_element_by_css_selector('.js_suspend').click()
			waitElementor('#suspend_button_alert',2)
			driver.find_element_by_css_selector('#suspend_button_alert').click()
			waitForElement('#confirm_button', 2)
			driver.find_element_by_css_selector('#confirm_button').click()
			log('ANUNCIO SUSPENSO COM SUCESSO')		
			msg = "SUSPENDER ANUNCIO: "+url
			send_notification(2, msg)
			requests.post(url_api + '/atualizar_link/' + id_annuncio,proxies=proxies)
			requests.post(url_api + '/update_anuncio/' + id_annuncio + '/' + str(idp) + '/5',proxies=proxies)
			requests.get(url_api + '/confirmar/'+str(id_anuncio)+'/1',proxies=proxies)
			driver.quit()
			sys.exit(1)	
		except Exception as ex:
			log(str(ex), 'ERRO', 'ERRO AO SUSPENDER')
			save_log(id_anuncio, 'ERRORE:  ANUNCIO NON TROVATO. / -> '+str(ex))
			requests.get(url_api + '/tentativa/'+str(id_anuncio),proxies=proxies)
			driver.quit()
			sys.exit(1)	
	except Exception as ex:
		log(str(ex), 'ERRO', 'ANUNCIO JA SUSPENSO')
		save_log(id_anuncio, 'ANUNCIO JA SUSPENSO')
		requests.post(url_api + '/update_anuncio/' + id_annuncio + '/' + str(idp) + '/5',proxies=proxies)
		requests.get(url_api + '/confirmar/'+str(id_anuncio)+'/1',proxies=proxies)
		#requests.get(url_api + '/tentativa/'+str(id_anuncio),proxies=proxies)
		driver.quit()
		sys.exit(1)	


log('INICIANDO')
parser = argparse.ArgumentParser()
parser.add_argument('--idp', required=True)
parser.add_argument('--id', required=True)
parser.add_argument('--id_annuncio', required=True)

args = parser.parse_args()
# ===========================================================================================
# COMO UTILIZAR
# utilizar atributos no shell como acima, ex: python run.py -idp=394023948203948 -indrizzo="EOFNPE WEOFNWPEOFN" ....
init(args.idp, args.id, args.id_annuncio)
driver.quit()
